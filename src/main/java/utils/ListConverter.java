package utils;

import models.ProductTypeCommonModel;

import java.util.ArrayList;
import java.util.List;

public class ListConverter {

    //was an idea to determine if response has specific field
    // for object model to downcast - but response fields are the same for each product type
    // and no specific value for each product type
    public <T extends ProductTypeCommonModel> List<T> getInstancesOfType(List<T> models, Class<T> type) {
        List<T> instances = new ArrayList<>();
        for (T model : models) {
            if (type.isInstance(model)) {
                instances.add(type.cast(model));
            }
        }
        return instances;
    }
}
