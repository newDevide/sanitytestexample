package models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Objects;

@Data
public class ProductTypeCommonModel {

    @JsonProperty("provider")
    private String provider;

    @JsonProperty("title")
    private String title;

    @JsonProperty("url")
    private String url;

    @JsonProperty("brand")
    private String brand;

    @JsonProperty("price")
    private double price;

    @JsonProperty("unit")
    private String unit;

    @JsonProperty("isPromo")
    private boolean promo;

    @JsonProperty("promoDetails")
    private String promoDetails;

    @JsonProperty("image")
    private String image;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductTypeCommonModel that = (ProductTypeCommonModel) o;
        return Double.compare(that.price, price) == 0
                && promo == that.promo
                && Objects.equals(provider, that.provider)
                && Objects.equals(title, that.title)
                && Objects.equals(url, that.url)
                && Objects.equals(brand, that.brand)
                && Objects.equals(unit, that.unit)
                && Objects.equals(promoDetails, that.promoDetails)
                && Objects.equals(image, that.image);
    }

    @Override
    public int hashCode() {
        return Objects.hash(provider, title, url, brand, price, unit, promo, promoDetails, image);
    }

    @Override
    public String toString() {
        return "ProductTypeCommonModel{" +
                "provider='" + provider + '\'' +
                ", title='" + title + '\'' +
                ", url='" + url + '\'' +
                ", brand='" + brand + '\'' +
                ", price=" + price +
                ", unit='" + unit + '\'' +
                ", isPromo=" + promo +
                ", promoDetails='" + promoDetails + '\'' +
                ", image='" + image + '\'' +
                '}';
    }
}
