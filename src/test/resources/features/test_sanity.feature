Feature: Search for the product

  Scenario: Searching for a orange product
    When "user" calls endpoint "orange"
    Then "user" sees the results displayed for productType: orange

  Scenario: Searching for a apple product
    When "user" calls endpoint "apple"
    Then "user" sees the results displayed for productType: apple

  Scenario: Searching for a pasta product
    When "user" calls endpoint "pasta"
    Then "user" sees the results displayed for productType: pasta

  Scenario: Searching for a cola product
    When "user" calls endpoint "cola"
    Then "user" sees the results displayed for productType: cola

  Scenario Outline: Searching for a product
    When call "<product>" with wrong method "<method>"
    Then getting error message "<error_response>"

    Examples:
      |  product   | method   | error_response       |
      |  orange    | POST     | Method Not Allowed   |
      |  orange    | DELETE   | Method Not Allowed   |
      |  apple     | POST     | Method Not Allowed   |
      |  apple     | DELETE   | Method Not Allowed   |
      |  pasta     | POST     | Method Not Allowed   |
      |  pasta     | DELETE   | Method Not Allowed   |
      |  cola      | POST     | Method Not Allowed   |
      |  cola      | DELETE   | Method Not Allowed   |
