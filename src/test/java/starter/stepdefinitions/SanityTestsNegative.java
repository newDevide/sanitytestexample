package starter.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;

import static starter.stepdefinitions.BaseTest.BASE_URL;
import static org.junit.Assert.*;

public class SanityTestsNegative {

    private Response response;

    @When("call {string} with wrong method {string}")
    public void call_endpoint_with_wrong_method(String product, String method) {
        // Perform api call
        switch (method) {
            case "POST":
                response = SerenityRest.given().post(BASE_URL + product);
                break;
            case "DELETE":
                response = SerenityRest.given().delete(BASE_URL + product);
                break;
            default:
                response = null;
                break;
        }
    }

    @Then("getting error message {string}")
    public void getting_error_message(String error_message) {
        assertNotNull(response.jsonPath());
        assertEquals(error_message, response.jsonPath().get("detail"));
    }
}
