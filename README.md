### What was refactored:
if in few words - almost everything
1) update .pom deleted some not used dependencies
2) removed ALL unused config files across whole project, like: 
- GitHab Action pipline files
- Gradle setup as we already have Maven and for this demo it is more then enough
- LICENSE file
- unused java classes and packages
- e.t.c.
3) Fixed tests as they were check nothing and were designed just to make execution "green"
4) wrote new tests according to requirements
5) created some common models and utils which are helpful 
6) left 1 test to be failed

### Project tech stack:
- Java 17
- Maven 3.8.6
- Serenity BDD testing
- Cucumber
- log4j
- lombok
- junit
- RestAssured

### How to setup project:
1) Clone repository from [GitLab Artifact](git@gitlab.com:newDevide/sanitytestexample.git)
- using ssh key for your account or via login credentials (access should be granted already if you're reading this)
2) Install and configure locally Java14 or higher and latest Maven build tool
3) Open project using any software development environment, like intellij idea or visual studio, e.t.c.
4) Set-up your Java for project SKD setting, the same shoukd be done for Maven 
5) Check than Maven downloaded all dependencies after some time and don't see any errors in logs
6) Go to the root foler of project in terminal and execute command 
```mvn clean verify```
7) This command should create class paths by classloaders and build all dependencies together.
Also it will trigger all tests we have currently in project and generate report in "target" folder

### How to write new tests
1) In order to add new tests we need first to describe them using gherkin format in ".feature" file
(src/test/resources/features/test_sanity.feature) - example below

```Gherkin
Feature: Search by keyword

  Scenario: Searching for a term
    Given Sergey is researching things on the internet
    When he looks up "Cucumber"
    Then he should see information about "Cucumber"
```

2) After we need to create appropriate java methods according to described steps:

```java
    @Given("{actor} is researching things on the internet")
    public void researchingThings(Actor actor) {
        actor.wasAbleTo(NavigateTo.theWikipediaHomePage());
    }

    @When("{actor} looks up {string}")
    public void searchesFor(Actor actor, String term) {
        actor.attemptsTo(
                LookForInformation.about(term)
        );
    }

    @Then("{actor} should see information about {string}")
    public void should_see_information_about(Actor actor, String term) {
        actor.attemptsTo(
                Ensure.that(WikipediaArticle.HEADING).hasText(term)
        );
    }
```

3) To run tests - ```mvn clean verify```

for more official information please refer to - src/test/README(OFFICIAL).md file

### Reporting
Each time you push in "main" GitLab Ci\CD starts from this file ".gitlab-ci.yml"
It runs tests and collect report
Fresh report available by GitLab Pages functionality, link below:
https://newdevide.gitlab.io/sanitytestexample/index.html 